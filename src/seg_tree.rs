pub trait SegTreeSpec {
    type F;
    type M: Clone;

    fn f(a: &Self::M, b: &Self::M) -> Self::M;
    fn identity() -> Self::M;
}

pub enum RSQ {}

impl SegTreeSpec for RSQ {
    type F = i64;
    type M = i64;
    fn f(a: &Self::M, b: &Self::M) -> Self::M {
        a + b
    }
    fn identity() -> Self::M {
        0
    }
}

pub struct SegTree<T: SegTreeSpec> {
    val: Vec<T::M>,
}

impl<T: SegTreeSpec> SegTree<T> {
    pub fn new(init_arr: &[T::M]) -> Self {
        let size = init_arr.len();
        let mut val = vec![T::identity(); size];
        val.extend_from_slice(init_arr);

        for p in (0..size).rev() {
            val[p] = T::f(&val[p << 1], &val[p << 1 | 1]);
        }

        Self { val }
    }

    pub fn modify(&mut self, mut k: usize, val: T::M) {
        k += self.val.len() / 2;

        self.val[k] = val;

        while k > 1 {
            self.val[k >> 1] = T::f(&self.val[k], &self.val[k ^ 1]);
            k >>= 1;
        }
    }

    pub fn query(&self, mut l: usize, mut r: usize) -> T::M {
        let mut ret = T::identity();
        l += self.val.len() / 2;
        r += self.val.len() / 2;

        while l < r {
            if l & 1 == 1 {
                ret = T::f(&ret, &self.val[l]);
                l += 1;
            }
            if r & 1 == 1 {
                r -= 1;
                ret = T::f(&ret, &self.val[r]);
            }
            l >>= 1;
            r >>= 1;
        }

        ret
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let n = 15;
        let mut arr = vec![];
        for i in 0..n {
            arr.push(i);
        }
        let mut t = SegTree::<RSQ>::new(&arr);

        let bf = t.query(0, 2);
        t.modify(0, 1);
        assert_eq!(bf + 1, t.query(0, 2));

        assert_eq!(t.query(3, 6), 12);

        t.modify(3, 5);
        assert_eq!(t.query(3, 6), 14);
    }
}
