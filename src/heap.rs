use std::mem;
use std::ptr;

#[derive(Debug, Default)]
pub struct MaxHeap<T> {
    data: Vec<T>,
}

impl<T: Ord> MaxHeap<T> {
    pub fn new() -> Self {
        Self { data: vec![] }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            data: Vec::with_capacity(capacity),
        }
    }

    pub fn peek(&self) -> Option<&T> {
        self.data.get(0)
    }

    /// `O(log n)` time complexity
    pub fn pop(&mut self) -> Option<T> {
        self.data.pop().map(|mut item| {
            if !self.is_empty() {
                mem::swap(&mut item, &mut self.data[0]);
                self.sift_down_to_bottom(0);
            }
            item
        })
    }

    /// `O(log n)` time complexity
    pub fn push(&mut self, item: T) {
        let old_len = self.len();
        self.data.push(item);
        self.sift_up(0, old_len);
    }

    fn sift_up(&mut self, start: usize, mut pos: usize) -> usize {
        unsafe {
            let item = ptr::read(self.data.get_unchecked(pos));
            while pos > start {
                let parent = (pos - 1) / 2;
                if item <= self.data[parent] {
                    break;
                }
                let parent_ptr: *const _ = self.data.get_unchecked(parent);
                let pos_ptr = self.data.get_unchecked_mut(pos);
                ptr::copy_nonoverlapping(parent_ptr, pos_ptr, 1);
                pos = parent;
            }
            self.data[pos] = item;
        }
        pos
    }

    fn sift_down_range(&mut self, mut pos: usize, end: usize) {
        unsafe {
            let item = ptr::read(self.data.get_unchecked(pos));
            let mut child = 2 * pos + 1;
            while child < end {
                let right = child + 1;
                if right < end && self.data[child] <= self.data[right] {
                    child = right;
                }
                if item >= self.data[child] {
                    break;
                }
                let child_ptr: *const _ = self.data.get_unchecked(child);
                let pos_ptr = self.data.get_unchecked_mut(pos);
                ptr::copy_nonoverlapping(child_ptr, pos_ptr, 1);
                pos = child;
                child = 2 * pos + 1;
            }
        }
    }

    fn sift_down(&mut self, pos: usize) {
        let len = self.len();
        self.sift_down_range(pos, len);
    }

    /// This is faster when the element is known to be large.
    fn sift_down_to_bottom(&mut self, mut pos: usize) {
        let end = self.len();
        let start = pos;
        unsafe {
            let mut child = 2 * pos + 1;
            while child < end {
                let right = child + 1;
                if right < end && self.data[child] <= self.data[right] {
                    child = right;
                }
                let child_ptr: *const _ = self.data.get_unchecked(child);
                let pos_ptr = self.data.get_unchecked_mut(pos);
                ptr::copy_nonoverlapping(child_ptr, pos_ptr, 1);
                pos = child;
                child = 2 * pos + 1;
            }
        }
        self.sift_up(start, pos);
    }

    /// `O(n)` time complexity
    fn rebuild(&mut self) {
        let mut n = self.len() / 2;
        while n > 0 {
            n -= 1;
            self.sift_down(n);
        }
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// `O(n log n)` time complexity
    pub fn into_sorted_vec(mut self) -> Vec<T> {
        let mut end = self.len();
        while end > 1 {
            end -= 1;
            self.data.swap(0, end);
            self.sift_down_range(0, end);
        }
        self.data
    }
}

impl<T: Ord> From<Vec<T>> for MaxHeap<T> {
    /// `O(n)` time complexity
    fn from(vec: Vec<T>) -> Self {
        let mut heap = Self { data: vec };
        heap.rebuild();
        heap
    }
}
