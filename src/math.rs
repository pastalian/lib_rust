pub fn binomial(n: usize, k: usize) -> usize {
    if k > n {
        return 0;
    }
    (0..k).fold(1, |s, k| s * (n - k) / (k + 1))
}
