pub fn largest_hist_rectangle(a: &mut Vec<i32>) -> i32 {
    let mut area = 0;
    let mut stack = vec![(a[0], 0)];

    a.push(0);
    for i in 0..a.len() {
        let last = stack.last().unwrap().0;
        if last < a[i] {
            stack.push((a[i], i));
        } else if last > a[i] {
            let mut target = i;
            while !stack.is_empty() && stack.last().unwrap().0 >= a[i] {
                let rect = stack.pop().unwrap();
                area = area.max(rect.0 * (i - rect.1) as i32);
                target = rect.1;
            }
            stack.push((a[i], target));
        }
    }
    area
}
